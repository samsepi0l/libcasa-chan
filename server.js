const http = require("http");
const fs = require("fs");

const server = http.createServer((req, res) => {
	if (req.url === "/styles/styles.css" ) {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/css");
		fs.readFile("styles/styles.css", function(error, data) {
			res.write(data);
			res.end();
		});
	}

	if (req.url === "/") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/index.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}
			res.end();
		});
	}

	if (req.url === "/about") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/about.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}
			res.end();
		});
	}

	if (req.url === "/register") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/register.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/libcasa-irc") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/libcasa.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

});

const port = process.env.port || 8000;

server.listen(port, () => console.log(`Server running on port ${port}`));


